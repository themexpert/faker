<?php

namespace ThemeXpert\Listeners;

use ThemeXpert\Events\UserWasRegistered;

class SendWelcomeEmail
{
    /**
     * To listen event
     *
     * @param UserWasRegistered $userWasRegistered
     */
    public function listen(UserWasRegistered $userWasRegistered)
    {
        var_dump($userWasRegistered);
    }
}